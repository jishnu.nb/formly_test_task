import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-flexlaout-forms-dynamic',
  templateUrl: './flexlayout-dynamic.component.html',
  styles: ['./flexlayout-dynamic.component.css']
})
export class FlexLayoutDynamicFormsComponent implements OnInit {
  form = new FormGroup({});
  model: any = {};
  options: FormlyFormOptions = {};

  fields: FormlyFieldConfig[] = [
    {
      // type: 'dropdown-dev',
      key: 'CowsMilk',
      templateOptions: {
        label: 'Additional analysis options',
      },
      fieldGroup: [
        {
          wrappers: ['panel'],
          templateOptions: { label: 'General' },

          fieldGroup: [{
            key: 'key',
            type: 'input',
            className: 'flex-1',
            templateOptions: {
              required: true,
              type: 'text',
              label: 'Town',
            },
          },
          {
            key: 'val_1',
            type: 'input',
            className: 'flex-1',
            templateOptions: {
              required: true,
              type: 'text',
              label: 'Town',
            },
          }],
        },
        {
          wrappers: ['panel'],
          templateOptions: { label: 'Dynamic' },
          fieldGroup: [{
            key: 'key_2',
            type: 'input',
            className: 'flex-1',
            templateOptions: {
              required: true,
              type: 'text',
              label: 'Town',
            },
          },
          {
            key: 'val_2',
            type: 'input',
            className: 'flex-1',
            templateOptions: {
              required: true,
              type: 'text',
              label: 'Town',
            },
          }
          ],
        },
      ]
    }

  ];

  constructor(private toastr: ToastrService) { }

  ngOnInit() { }

  submit() {
    this.showToast();
  }

  showToast() {
    this.toastr.success(JSON.stringify(this.model));
  }
}
