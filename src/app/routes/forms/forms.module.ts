import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { FormsRoutingModule } from './forms-routing.module';

import { FormsElementsComponent } from './elements/elements.component';
import { FormsSelectsComponent } from './selects/selects.component';
import { FormsDynamicComponent } from './dynamic/dynamic.component';
import { FlexLayoutDynamicFormsComponent } from './flexlayout-dynamic/flexlayout-dynamic.component';
import { PanelWrapperComponent } from './flexlayout-dynamic/panel-wrapper.component';
import { FormlyModule } from '@ngx-formly/core';
import { PanelDropdownDevType } from './flexlayout-dynamic/dropdown-dev.type';

const COMPONENTS = [FormsElementsComponent, FormsSelectsComponent, FormsDynamicComponent, FlexLayoutDynamicFormsComponent];
const COMPONENTS_DYNAMIC = [PanelWrapperComponent,PanelDropdownDevType];

@NgModule({
  imports: [SharedModule, FormsRoutingModule,
    FormlyModule.forRoot({
      types: [
        { name: 'dropdown-dev', component: PanelDropdownDevType }
      ],
      wrappers: [
        { name: 'panel', component: PanelWrapperComponent },
      ],
    }),],
  declarations: [...COMPONENTS, ...COMPONENTS_DYNAMIC],
  entryComponents: COMPONENTS_DYNAMIC,
})
export class FormsModule { }
